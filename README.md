## Magento 2.3中文语言包

今天，我将为您提供另一个有用的文档-** Magento 2中文简体语言包**。通过此知识库文章，您只需两个简单的步骤即可将Magento 2默认语言更改为简体中文。完成后，店面中的所有标签都将自动快速转换为简体中文。

##概述

1.语言打包过程
2.安装中文语言包
3.如何激活中文语言包
4.如何贡献
5.支持的Magento版本
6.注意事项
7.语言包作者


## 1.如何安装中文语言包

有2种不同的方法来安装此语言包。

###✓方法＃1。作曲者方法（推荐）
通过composer安装中文包从未如此简单。

**安装中文包**：

```
composer require GDTang1220/magento-2.3-chinese-language-pack:"2.0"
php bin/magento setup:static-content:deploy zh_Hans_CN
php bin/magento indexer:reindex
php bin/magento cache:clean
php bin/magento cache:flush

```


**更新中文包**：

```
composer update GDTang1220/magento-2.3-chinese-language-pack:"2.0"
php bin/magento setup:static-content:deploy zh_Hans_CN
php bin/magento indexer:reindex
php bin/magento cache:clean
php bin/magento cache:flush

```

####需要验证（如果有）

！[需要验证]（https://cdn.mageplaza.com/media/general/dmryiPk.png）

如果尚未添加此身份验证，则可以遵循[本指南]（http://devdocs.magento.com/guides/v2.0/install-gde/prereq/connect-auth.html）

或使用以下键：

```
公钥：c7af1bfc9352e9c986637eec85ed53af
私钥：17e1b72ea5f0b23e9dbfb1f68dc12b53
```



###✓方法2。复制和粘贴方法（不推荐）

此方法适用于非技术人员，例如商人。只需下载软件包，然后刷新缓存即可。

**概述**

-步骤1：下载中文语言包
-步骤2：将中文包解压缩
-步骤3：清空Magento 2缓存

####步骤1：下载中文语言包

您可以从上面的链接下载语言包

####步骤2：解压缩中文包

将中文包解压缩到Magento 2根文件夹。在本指南中，我们提取到`/ var / www / html /`
您的Magento 2根文件夹可以是：/home/account_name/yourstore.com/public_html/

```
解压master.zip app / i18n / Mageplaza /
```

将文件夹“ magento-2-chinese-language-pack”重命名为“ zh_hans_cn”。


您还可以在本地解压缩并将其上传到Magento 2根文件夹。

####步骤3：清空Magento 2缓存

遵循本指南[在您的Magento 2商店中刷新缓存]（https://www.mageplaza.com/kb/how-flush-enable-disable-cache.html）